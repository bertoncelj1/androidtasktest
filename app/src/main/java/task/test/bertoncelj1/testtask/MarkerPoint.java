package task.test.bertoncelj1.testtask;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by bertoncelj1 on 17.9.2015.
 */



class MarkerPoint{

    public enum MarkerVisibility {HIGH, MEDIUM, SMALL};

    private final float ZOOM_LEVEL_COUNTRY = 5;
    private static final float ZOOM_LEVEL_REGION = 12;
    private static final float ZOOM_LEVEL_STREET = 14;

    Marker marker;
    MarkerVisibility visibility;
    private boolean tracked;

    //variables from API
    int id;
    String name;
    String pin_image_url;
    Bitmap pin_image;
    LatLng position;

    //TODO create option class ?
    public MarkerPoint(){
        this.position = null;
        this.marker = null;
        this.visibility = MarkerVisibility.MEDIUM;
        this.tracked = false;
    }

    public String getTitle(){
        switch(visibility) {
            case HIGH:
                return "High";
            case MEDIUM:
                return "Medium";
            case SMALL:
                return "Low";
        }
        return "Not defined";
    }

    public void setPinImage(Bitmap bitmap){
        pin_image = bitmap;
        marker.setIcon(BitmapDescriptorFactory.fromBitmap(pin_image));
    }

    public void setMarker(Marker _marker){
        this.marker = _marker;
        marker.setIcon(BitmapDescriptorFactory.defaultMarker(getColorHue()));
        marker.setSnippet(getTitle());
    }

    public void setTracked(boolean tracked){
        this.tracked = tracked;
        //marker.setIcon(BitmapDescriptorFactory.defaultMarker(getColorHue()));
    }
    public float getColorHue(){
        if(tracked)return BitmapDescriptorFactory.HUE_BLUE;

        switch(visibility) {
            case HIGH:
                return BitmapDescriptorFactory.HUE_RED;
            case MEDIUM:
                return BitmapDescriptorFactory.HUE_ORANGE;
            case SMALL:
                return BitmapDescriptorFactory.HUE_YELLOW;
        }
        return BitmapDescriptorFactory.HUE_GREEN;
    }

    public boolean isVisible(float zoom){
        if(tracked) return true;


        switch(visibility) {
            case HIGH:
                return zoom > ZOOM_LEVEL_COUNTRY;
            case MEDIUM:
                return zoom > ZOOM_LEVEL_REGION;
            case SMALL:
                return zoom > ZOOM_LEVEL_STREET;
        }
        return true;
    }

    public Marker getMarker() {
        return marker;
    }

    public boolean isTracked() {
        return tracked;
    }
}


