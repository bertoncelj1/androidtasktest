package task.test.bertoncelj1.testtask;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;


import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by bertoncelj1 on 9/22/15.
 */
public class TripkoAPIManager {

    private static final String username = "anze";
    private static final String password = "anze";
    private static final String token = "a3d4776fc6d32933d2afa1a92c541444";

    public ArrayList<MarkerPoint> markerPoints = new ArrayList<>();
    boolean userAuth = false;
    Header[] mCookies = null;
    Header[] requiredHeaders = {
            new BasicHeader("Accept", "*/*"),
            new BasicHeader("Accept-Encoding:", "gzip,deflate"),                    //TODO server zahteva eno od teh polji ?? bug feture ?
            new BasicHeader("Accept-Language:", "sl,en-GB;q=0.8,en;q=0.6,de;q=0.4"),
            new BasicHeader("Authorization", "Token token=\"" + token + "\"")
    };

    GoogleMap map;

    public TripkoAPIManager(GoogleMap map){
        this.map = map;

        authenticateUser();
    }

    private TripkoAPIManagerOnError onErrorFunction = null;
    public interface TripkoAPIManagerOnError{
        void TripkoAPIManagerError(String message);
    }
    public void setOnErrorListener(TripkoAPIManagerOnError OnFinishCallback) {
        this.onErrorFunction = OnFinishCallback;
    }
    private void operationError(String message){
        if(onErrorFunction != null){
            onErrorFunction.TripkoAPIManagerError(message);
        }
    }

    private static final String languageId = "20";
    private String getDumpPoisFromGPSUrl(LatLng position) {
        String latitude = "lat=" + position.latitude;
        String longitude = "lon=" + position.longitude;
        String language = "lang_id=" + languageId;

        return "http://api.tripko.si/api/pois/dump_pois_from_gps?" +
                language + "&" +
                longitude + "&" +
                latitude;
    }

    private String getAuthenticateUserUrl() {
        return "http://api.tripko.si/api/users/authenticate_user";
    }

    private String getAuthenticateUserJSON() {
        return "{" +
                "\"username\":\""+ username +"\"," +
                "\"password\":\""+ password +"\"" +
                "}";
    }

    public void setNewMarkerPoints(LatLng position) {
        if(userAuth) {
            String url = getDumpPoisFromGPSUrl(position);
            new setAPIpointsTask().execute(url);
        }else{
            authenticateUser();
        }
    }

    public void showMarkers(float zoom) {
        for(MarkerPoint markerPoint: markerPoints){
            markerPoint.getMarker().setVisible(markerPoint.isVisible(zoom));
        }
    }
    public MarkerPoint getMarkerPoint(Marker marker){
        for(MarkerPoint markerPoint: markerPoints){
            if(marker.hashCode() == markerPoint.getMarker().hashCode())return markerPoint;
        }
        return null;
    }


    private class setAPIpointsTask extends AsyncTask<String, Void, String>{
        @Override
        protected String doInBackground(String... params) {

            Log.d("API", "Set Points ...");

            HttpClient httpClient = new DefaultHttpClient();

            // In a POST request, we don't pass the values in the URL.
            //Therefore we use only the web page URL as the parameter of the HttpPost argument
            String url = params[0];
            HttpGet httpGet = new HttpGet(url);

            httpGet.setHeaders(requiredHeaders);
            for(Header cookie: mCookies){
                httpGet.addHeader("Cookie", cookie.getValue());
                Log.d("API", "Added cookie: " + cookie.getValue());
            }

            try {
                // HttpResponse is an interface just like HttpPost.
                //Therefore we can't initialize them
                HttpResponse httpResponse = httpClient.execute(httpGet);

                // According to the JAVA API, InputStream constructor do nothing.
                //So we can't initialize InputStream although it is not an interface
                InputStream inputStream = httpResponse.getEntity().getContent();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                StringBuilder stringBuilder = new StringBuilder();

                String bufferedStrChunk;

                while((bufferedStrChunk = bufferedReader.readLine()) != null){
                    stringBuilder.append(bufferedStrChunk);
                }

                return stringBuilder.toString();

            } catch (ClientProtocolException cpe) {
                Log.d("API", "First Exception caz of HttpResponese :" + cpe);
                cpe.printStackTrace();
            } catch (IOException ioe) {
                Log.d("API", "Second Exception caz of HttpResponse :" + ioe);
                ioe.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Log.d("API", "HTTP GET " + result);
            new ParserTask().execute(result);

        }
    }

    private class ParserTask extends
            AsyncTask<String, Void, ArrayList<MarkerPoint>> {

        @Override
        protected ArrayList<MarkerPoint> doInBackground(
                String... jsonData) {

            JSONObject jObject;
            ArrayList<MarkerPoint> points = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                points =  createMarkerPoints(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return points;
        }

        @Override
        protected void onPostExecute(ArrayList<MarkerPoint> points) {
            markerPoints = points;

            // traversing through marker points
            for (int i = 0; i < markerPoints.size(); i++) {

                MarkerPoint mp = markerPoints.get(i);
                mp.marker = map.addMarker(new MarkerOptions()
                        .position(mp.position)
                        .title(mp.name + "")
                        .visible(mp.isVisible(map.getCameraPosition().zoom)));

                new DownloadImageTask(mp).execute("http://api.tripko.si" + mp.pin_image_url);
            }
            operationError("Points set");
        }
    }

    Random r = new Random();
    private MarkerPoint.MarkerVisibility getRandomVisibility(){
        int randomInt = r.nextInt(100);
        if(randomInt < 20)return MarkerPoint.MarkerVisibility.HIGH;
        if(randomInt < 60)return MarkerPoint.MarkerVisibility.MEDIUM;

        return MarkerPoint.MarkerVisibility.SMALL;
    }
    public ArrayList<MarkerPoint> createMarkerPoints(JSONObject jObject) {
        ArrayList<MarkerPoint> mPoints = new ArrayList<>();

        try {
            JSONArray jData = jObject.getJSONArray("data");

            /** Traversing all data */
            for (int i = 0; i < jData.length(); i++) {
                JSONObject jPoi =  (JSONObject) ((JSONArray)
                        ((JSONObject) jData.get(i)).get("poi")).get(0);


                MarkerPoint mk = new MarkerPoint();
                mk.id = jPoi.getInt("id");
                mk.name = jPoi.getString("name");
                mk.position = new LatLng(jPoi.getDouble("lat"), jPoi.getDouble("lon"));
                mk.pin_image_url = jPoi.getString("pin_small");
                mk.visibility = getRandomVisibility();

                //Log.d("API", "New Point id:" + mk.id);
                mPoints.add(mk);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mPoints;
    }

    public void authenticateUser() {

        class SendPostReqAsyncTask extends AsyncTask<String, Void, String>{

            @Override
            protected String doInBackground(String... params) {


                Log.d("API", "Authenticating user...");

                HttpClient httpClient = new DefaultHttpClient();

                // In a POST request, we don't pass the values in the URL.
                //Therefore we use only the web page URL as the parameter of the HttpPost argument
                String url = getAuthenticateUserUrl();
                HttpPost httpPost = new HttpPost(url);


                String jsonString = getAuthenticateUserJSON();


                httpPost.setHeaders(requiredHeaders);
                httpPost.addHeader("Content-type", "application/json");


                try {

                    httpPost.setEntity(new StringEntity(jsonString, "UTF8"));

                    try {
                        // HttpResponse is an interface just like HttpPost.
                        //Therefore we can't initialize them
                        HttpResponse httpResponse = httpClient.execute(httpPost);

                        mCookies = httpResponse.getHeaders("Set-Cookie");
                        for (Header mCooky : mCookies) {
                            Log.d("API", "Cookie: " + mCooky.getValue());
                        }

                        // According to the JAVA API, InputStream constructor do nothing.
                        //So we can't initialize InputStream although it is not an interface
                        InputStream inputStream = httpResponse.getEntity().getContent();

                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                        StringBuilder stringBuilder = new StringBuilder();

                        String bufferedStrChunk;

                        while((bufferedStrChunk = bufferedReader.readLine()) != null){
                            stringBuilder.append(bufferedStrChunk);
                        }

                        return stringBuilder.toString();

                    } catch (ClientProtocolException cpe) {
                        Log.d("API", "First Exception caz of HttpResponese :" + cpe);
                        operationError("Failed to access internet");
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        Log.d("API", "Second Exception caz of HttpResponse :" + ioe);
                        operationError("Failed to access internet");
                        ioe.printStackTrace();
                    }

                } catch (UnsupportedEncodingException uee) {
                    Log.d("API", "An Exception given because of UrlEncodedFormEntity argument :" + uee);
                    operationError("Failed to access internet");
                    uee.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);

                if(result != null) {
                    Log.d("API", "User authentication result:" + result);
                    if (result.contains("User authenticated")) {
                        Log.d("API", "User authenticated!:" + result);
                        userAuth = true;
                    }
                }else{
                    operationError("Failed to access server");
                }

            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute();
    }

    public void setImage(MarkerPoint marker){
        new DownloadImageTask(marker).execute("http://api.tripko.si/pins/1_100px.png");
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        MarkerPoint markerPoint;

        public DownloadImageTask(MarkerPoint marker) {
            this.markerPoint = marker;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;

            HttpGet httpGet = new HttpGet(urldisplay);

            httpGet.setHeaders(requiredHeaders);
            for(Header cookie: mCookies){
                httpGet.addHeader("Cookie", cookie.getValue());
                Log.d("API", "Added cookie: " + cookie.getValue());
            }

            try {
                HttpClient client = new DefaultHttpClient();
                HttpResponse httpResponse = client.execute(httpGet);
                InputStream in = httpResponse.getEntity().getContent();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            markerPoint.setPinImage(result);
        }
    }



}
