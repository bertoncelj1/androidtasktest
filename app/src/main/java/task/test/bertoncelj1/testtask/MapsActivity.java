package task.test.bertoncelj1.testtask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements
        OnMapReadyCallback,
        GoogleMap.OnCameraChangeListener,
        GoogleMap.OnMarkerClickListener,
        LocationListener,
        TripkoAPIManager.TripkoAPIManagerOnError,
        GoogleApiClient.ConnectionCallbacks {


    private GoogleMap mMap;
    private final Activity activity = this;
    private GoogleApiClient mGoogleApiClient;
    ArrayList<MarkerPoint> trackedMarkers = new ArrayList<>();
    DirectionPlaner myPlanner = new DirectionPlaner();
    TripkoAPIManager APImng;

    public static final CameraPosition OSREDNJA_SLOVENIJA =
            new CameraPosition.Builder().target(new LatLng(46.04673, 14.50026))
            .zoom(11)
            .bearing(0)
            .tilt(0)
            .build();

    // These settings are the same as the settings for the map. They will in fact give you updates
    // at the maximal rates currently possible.
    private static final LocationRequest REQUEST = LocationRequest.create()
            .setInterval(5000)         // 5 seconds
            .setFastestInterval(16)    // 16ms = 60fps
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        this.setProgressBarIndeterminateVisibility(false);

        setContentView(R.layout.activity_maps);
        initMap(savedInstanceState);

    }

    private void initMap(Bundle savedInstanceState){
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        if(savedInstanceState == null){
            // First incarnation of this activity
            mapFragment.setRetainInstance(true);
        }
        mapFragment.getMapAsync(this);

        myConnectionCallbacks cb = new myConnectionCallbacks();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(cb)
                .build();
    }


    private void addMarkers(LatLng position) {
        setProgressBarIndeterminateVisibility(true);
        APImng.setNewMarkerPoints(position);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        APImng = new TripkoAPIManager(mMap);
        APImng.setOnErrorListener(this);
        mMap.setMyLocationEnabled(true);
        mMap.setOnCameraChangeListener(this);
        mMap.setOnMarkerClickListener(this);

        //addMarkers()

        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(OSREDNJA_SLOVENIJA));
    }


    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        APImng.showMarkers(cameraPosition.zoom);
    }


    private boolean isDialogShown = false;
    @Override
    public boolean onMarkerClick(Marker marker) {
        MarkerPoint markerPoint = APImng.getMarkerPoint(marker);
        if(markerPoint == null)return false;

        if(!isDialogShown) {
            isDialogShown = true;
            showTrackedDialog(markerPoint);
        }
        return true;
    }

    private void removeFromTrack(MarkerPoint markerPoint){
        if (markerPoint.isTracked()) trackedMarkers.remove(markerPoint);
        markerPoint.setTracked(false);
        markerPoint.getMarker().setVisible(markerPoint.isVisible(mMap.getCameraPosition().zoom));
        myPlanner.clearPath();
    }

    private void addToTrack(MarkerPoint markerPoint){
        markerPoint.setTracked(true);
        trackedMarkers.add(markerPoint);
        myPlanner.clearPath();
    }
    public void showTrackedDialog(final MarkerPoint markerPoint){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                .setCancelable(true)
                .setNegativeButton("Cancel", null);

        if (markerPoint.isTracked()) {
            dialog.setTitle("Remove from track?");
            dialog.setNeutralButton("Remove", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    removeFromTrack(markerPoint);
                }
            });
        } else {
            dialog.setTitle("Add to track?");
            dialog.setNeutralButton("Add", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    addToTrack(markerPoint);
                }
            });
        }


        //requires API 17!
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                isDialogShown = false;
            }
        });


        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_maps, menu);
        return true;
    }

    private void clearTrackedPoints(){
        for(MarkerPoint markerPoint: trackedMarkers){
            markerPoint.setTracked(false);
            markerPoint.getMarker().setVisible(markerPoint.isVisible(mMap.getCameraPosition().zoom));
        }
        trackedMarkers.clear();
    }
    private ArrayList<LatLng> getTrackPointsLatLng(){
        ArrayList<LatLng> points = new ArrayList<>();
        Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if(lastLocation != null) {
            points.add(new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude()));
        }

        for(MarkerPoint mp : trackedMarkers){
            points.add(mp.position);//TODO tuki bi bilo treba narediti nov objekt v primeru da se tracker point zbrise
        }

        return points;
    }
    private void showTrack(){
        if(trackedMarkers.size() >= 2){
            setProgressBarIndeterminateVisibility(true);
            myPlanner.setPoints(getTrackPointsLatLng());
            myPlanner.clearPath();
            myPlanner.drawDirections(mMap);

            myPlanner.setOnFinishListener(new DirectionPlaner.directionPlanerOnFinish() {
                @Override
                public void directionPlanerFinished(boolean error, String message) {
                    activity.setProgressBarIndeterminateVisibility(false);
                    if(error){
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }else{
            Toast.makeText(this, "Select at least 2 points", Toast.LENGTH_SHORT).show();
        }

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.clear:
                clearTrackedPoints();
                myPlanner.clearPath();
                return true;

            case R.id.show:
                showTrack();
                return true;

            case R.id.setMarkers:
                addMarkers(new LatLng(46.08961, 14.44944));
                return true;

            default:
                return false;
        }
    }

    boolean once = false;
    @Override
    public void onLocationChanged(Location location) {
        if(!once){
            mMap.clear();
            addMarkers(new LatLng(location.getLatitude(), location.getLongitude()));
        }
        once = true;
    }

    @Override
    public void TripkoAPIManagerError(final String message) {
        final Activity activity = this;
        //TODO spremeni koncept
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                setProgressBarIndeterminateVisibility(false);
            }//public void run() {
        });
    }

    @Override
    public void onConnected(Bundle bundle) {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                REQUEST,
                this);  // LocationListener
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    class myConnectionCallbacks implements
            GoogleApiClient.ConnectionCallbacks,
            GoogleApiClient.OnConnectionFailedListener {


        @Override
        public void onConnected(Bundle bundle) {

        }

        @Override
        public void onConnectionSuspended(int i) {

        }

        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {

        }
    }

}
