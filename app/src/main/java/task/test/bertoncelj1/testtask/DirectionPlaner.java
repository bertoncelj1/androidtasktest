package task.test.bertoncelj1.testtask;

import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by bertoncelj1 on 9/17/15.
 */
public class DirectionPlaner {

    ArrayList<LatLng> points;
    GoogleMap googleMap;

    Polyline polylinePath;
    private static final int PATH_WIDTH = 3;
    private static final int PATH_COLOR = Color.BLUE;

    public DirectionPlaner(){
    }

    public DirectionPlaner(ArrayList<LatLng> points){
        setPoints(points);
    }

    public void setPoints(ArrayList<LatLng> points){
        this.points = points;
    }

    public void clearPath(){
        if(polylinePath != null){
            polylinePath.remove();
        }
    }

    public void drawDirections(GoogleMap map){
        if(map == null || points == null || points.size() < 2)return;
        googleMap = map;

        ReadTask downloadTask = new ReadTask();

        String url = getMapsApiDirectionsUrl();
        downloadTask.execute(url);
    }

    private String getMapsApiDirectionsUrl() {

        String origin = "origin=" +
                points.get(0).latitude + "," +
                points.get(0).longitude;

        String waypoints = "waypoints=";
        int i = 1;
        while(i < points.size() - 2){
            waypoints += points.get(i).latitude + "," +
                    points.get(i).longitude + "|";
            i++;
        }
        if(i < points.size() - 1) {
            waypoints += points.get(i).latitude + "," +
                    points.get(i).longitude;
            i++;
        }

        String destination = "destination=" +
                points.get(i).latitude + "," +
                points.get(i).longitude;

        String mode = "mode=" + "walking";

        String sensor = "sensor=false";
        String params = origin + "&" + destination + "&" + waypoints + "&" + sensor + "&" + mode;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/"
                + output + "?" + params;
        Log.d("URL", url);
        return url;
    }

    private directionPlanerOnFinish OnFinishCallback = null;
    public interface directionPlanerOnFinish{
        void directionPlanerFinished(boolean error, String message);
    }
    public void setOnFinishListener(directionPlanerOnFinish OnFinishCallback) {
        this.OnFinishCallback = OnFinishCallback;
    }
    private void operationFinished(boolean error, String message){
        if(OnFinishCallback != null){
            OnFinishCallback.directionPlanerFinished(error, message);
        }
    }

    private class ReadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                HttpConnection http = new HttpConnection();
                data = http.readUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            new ParserTask().execute(result);
        }
    }

    private class ParserTask extends
            AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                routes =  new PathJSONParser().parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
            ArrayList<LatLng> points;
            PolylineOptions polyLineOptions = null;
            if(routes == null){
                operationFinished(true, "Failed to parse.");
                return;
            }

            // traversing through routes
            for (int i = 0; i < routes.size(); i++) {
                points = new ArrayList<LatLng>();
                polyLineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = routes.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                polyLineOptions.addAll(points);
                polyLineOptions.width(PATH_WIDTH);
                polyLineOptions.color(PATH_COLOR);
            }

            if(polyLineOptions != null) {
               polylinePath = googleMap.addPolyline(polyLineOptions);
                operationFinished(false, "Operation OK!");
            }else{
                operationFinished(true, "Failed to show route");
            }
        }
    }

    private class PathJSONParser {

        public List<List<HashMap<String, String>>> parse(JSONObject jObject) {
            List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String, String>>>();
            JSONArray jRoutes = null;
            JSONArray jLegs = null;
            JSONArray jSteps = null;
            try {
                jRoutes = jObject.getJSONArray("routes");
                /** Traversing all routes */
                for (int i = 0; i < jRoutes.length(); i++) {
                    jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
                    List<HashMap<String, String>> path = new ArrayList<HashMap<String, String>>();

                    /** Traversing all legs */
                    for (int j = 0; j < jLegs.length(); j++) {
                        jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");

                        /** Traversing all steps */
                        for (int k = 0; k < jSteps.length(); k++) {
                            String polyline = "";
                            polyline = (String) ((JSONObject) ((JSONObject) jSteps
                                    .get(k)).get("polyline")).get("points");
                            List<LatLng> list = decodePoly(polyline);

                            /** Traversing all points */
                            for (int l = 0; l < list.size(); l++) {
                                HashMap<String, String> hm = new HashMap<String, String>();
                                hm.put("lat",
                                        Double.toString(((LatLng) list.get(l)).latitude));
                                hm.put("lng",
                                        Double.toString(((LatLng) list.get(l)).longitude));
                                path.add(hm);
                            }
                        }
                        routes.add(path);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
                operationFinished(true, "Failed to show route");
            } catch (Exception e) {
            }
            return routes;
        }

        /**
         * Method Courtesy :
         * jeffreysambells.com/2010/05/27
         * /decoding-polylines-from-google-maps-direction-api-with-java
         * */
        private List<LatLng> decodePoly(String encoded) {

            List<LatLng> poly = new ArrayList<LatLng>();
            int index = 0, len = encoded.length();
            int lat = 0, lng = 0;

            while (index < len) {
                int b, shift = 0, result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lat += dlat;

                shift = 0;
                result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lng += dlng;

                LatLng p = new LatLng((((double) lat / 1E5)),
                        (((double) lng / 1E5)));
                poly.add(p);
            }
            return poly;
        }
    }

    private class HttpConnection {
        public String readUrl(String mapsApiDirectionsUrl) throws IOException {
            String data = "";
            InputStream iStream = null;
            HttpURLConnection urlConnection = null;
            try {
                URL url = new URL(mapsApiDirectionsUrl);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.connect();
                iStream = urlConnection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
                StringBuffer sb = new StringBuffer();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                data = sb.toString();
                br.close();
            } catch (Exception e) {
                Log.d("TAG", "Exception while reading url" + e.toString());
                operationFinished(true, "Failed to access internet");
            } finally {
                iStream.close();
                urlConnection.disconnect();
            }
            return data;
        }

    }
}


